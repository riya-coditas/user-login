"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_responses_1 = require("./user.responses");
const user_repo_1 = __importDefault(require("./user.repo"));
const create = (user) => {
    user_repo_1.default.create(user);
    return user_responses_1.UserResponses.USER_CREATED;
};
//const findAll=()=>userRepo.findAll();
const findAll = (password, username) => {
    //userRepo.findAll();
    const isfound = user_repo_1.default.findAll(password, username);
    if (isfound)
        return user_responses_1.UserResponses.USER_VALIDATED;
    // return UserResponses.USER_NOT_FOUND;
};
const update = (user) => {
    const didUpdate = user_repo_1.default.update(user);
    if (didUpdate)
        return user_responses_1.UserResponses.USER_UPDATED;
    return user_responses_1.UserResponses.USER_NOT_FOUND;
};
const remove = (id) => {
    const didDeleted = user_repo_1.default.remove(id);
    if (didDeleted)
        return user_responses_1.UserResponses.USER_DELETED;
    return user_responses_1.UserResponses.USER_NOT_FOUND;
};
exports.default = {
    create,
    findAll,
    update,
    remove
};

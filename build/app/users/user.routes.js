"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_service_1 = __importDefault(require("./user.service"));
const router = (0, express_1.Router)();
// router.get('/', (req, res, next) => {
//     try {
//         const result = userService.findAll();
//         res.send(result);
//     } catch (error) {
//         next(error)
//     }
// });
router.get('/:password/:username', (req, res, next) => {
    try {
        const { password, username } = req.params;
        // const {username} = req.params;
        const result = user_service_1.default.findAll(password, username);
        res.send(result);
    }
    catch (error) {
        next(error);
    }
});
router.post('/', (req, res, next) => {
    try {
        const result = user_service_1.default.create(req.body);
        res.send(result);
    }
    catch (error) {
        next(error);
    }
});
router.patch('/', (req, res, next) => {
    try {
        const result = user_service_1.default.remove(req.body);
        res.send(result);
    }
    catch (error) {
        next(error);
    }
});
router.delete('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        const result = user_service_1.default.remove(id);
        res.send(result);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;

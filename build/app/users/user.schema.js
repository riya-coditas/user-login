"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const uuid_1 = require("uuid");
class UserSchema {
    static create(user) {
        const id = (0, uuid_1.v4)();
        const userRecord = Object.assign(Object.assign({}, user), { id });
        UserSchema.users.push(userRecord);
        return userRecord;
    }
    static findAll(password, username) {
        const index = UserSchema.users.findIndex((u) => u.password === password && u.username === username);
        if (index === -1)
            return false;
        return true;
    }
    static update(user) {
        const index = UserSchema.users.findIndex((u) => u.id === user.id);
        if (index === -1)
            return false;
        UserSchema.users[index] = Object.assign(Object.assign({}, user), { id: UserSchema.users[index].id });
        return true;
    }
    static removeUser(id) {
        const index = UserSchema.users.findIndex((u) => u.id === id);
        if (index === -1)
            return false;
        UserSchema.users.splice(index, 1);
        return true;
    }
}
exports.UserSchema = UserSchema;
UserSchema.users = [];

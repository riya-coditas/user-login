"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResponses = void 0;
exports.UserResponses = {
    USER_NOT_FOUND: {
        statusCode: 404,
        message: "User not found"
    },
    USER_UPDATED: {
        statusCode: 200,
        message: "User updated successfully"
    },
    USER_DELETED: {
        statusCode: 200,
        message: "User deleted successfully"
    },
    USER_CREATED: {
        statusCode: 201,
        message: "User created successfully"
    },
    USER_VALIDATED: {
        statusCode: 201,
        message: "User validated successfully"
    },
};

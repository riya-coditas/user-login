"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const create = (user) => user_schema_1.UserSchema.create(user);
const findAll = (password, username) => user_schema_1.UserSchema.findAll(password, username);
const update = (user) => user_schema_1.UserSchema.update(user);
const remove = (id) => user_schema_1.UserSchema.removeUser(id);
exports.default = {
    create,
    findAll,
    update,
    remove
};

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const express_1 = __importDefault(require("express"));
const route_1 = require("./routes/route");
const createServer = () => {
    const app = (0, express_1.default)();
    const { PORT } = process.env;
    (0, route_1.registerRoutes)(app);
    app.listen(PORT || 4000, () => console.log(`started at :${PORT || 4000}`));
};
exports.createServer = createServer;

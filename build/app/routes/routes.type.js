"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
class Routes {
    constructor(path, handler) {
        this.path = path;
        this.handler = handler;
    }
}
exports.Routes = Routes;

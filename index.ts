import {config} from 'dotenv';
config();

import {createServer} from './app/app'
createServer();
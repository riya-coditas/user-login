import {Router} from "express";

class Routes{
    constructor(
        public path:string,
        public handler:Router
    ) {}
}

type Route = Routes[];
export {Route,Routes}
import { Application, json, NextFunction,Request,Response } from "express"
import { routes } from "./route.data"

export const registerRoutes = (app: Application) => {
    app.use(json())

    for (const iterator of routes) {
        app.use(iterator.path, iterator.handler)
    }

    app.use((error:any,re:Request,res:Response,next:NextFunction)=>{
        res.status(error.statusCode || 500).send(error);
    });
}
import {Route, Routes} from './routes.type'
import UserRouter from '../users/user.routes'

export const routes:Route=[
    new Routes('/user',UserRouter)
]
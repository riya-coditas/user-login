import express from 'express'
import { registerRoutes } from './routes/route';

export const createServer=()=>{
    const app = express();
    const {PORT} = process.env;
    registerRoutes(app)

    app.listen(
        PORT || 4000,
        () => console.log(`started at :${PORT || 4000}`)
    );
}
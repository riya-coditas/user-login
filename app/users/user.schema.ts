import { v4 } from "uuid";
import { Iuser, USER } from "./user.types";

export class UserSchema{
    private static users: USER=[];

    static create(user:Iuser):Iuser{
        const id=v4();
        const userRecord={...user,id}
        UserSchema.users.push(userRecord);

        return userRecord;
    }

    static findAll(password:string,username:string):boolean{
        const index=UserSchema.users.findIndex((u)=>
            u.password===password && u.username===username);
        
        if(index===-1) return false;
        return true;
    }

    static update(user:Iuser):boolean{
        const index=UserSchema.users.findIndex((u)=>u.id===user.id);
        if(index===-1) return false;
        UserSchema.users[index]={...user,id:UserSchema.users[index].id}
        return true;
    }
    
    static removeUser(id:string):boolean{
        const index=UserSchema.users.findIndex((u)=>u.id===id);
        if(index===-1) return false;
        UserSchema.users.splice(index,1);
        return true;
    }
}
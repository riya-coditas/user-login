import { UserResponses } from "./user.responses";
import  userRepo  from "./user.repo";
import { Iuser } from "./user.types";

const create=(user:Iuser)=>{
    userRepo.create(user);
    return UserResponses.USER_CREATED;
}
//const findAll=()=>userRepo.findAll();

const findAll=(password:string,username:string)=>{
    //userRepo.findAll();
    const isfound = userRepo.findAll(password,username);
    if(isfound) return UserResponses.USER_VALIDATED;
    return UserResponses.USER_NOT_FOUND;
}

const update=(user:Iuser)=>{
    const didUpdate=userRepo.update(user);
    if(didUpdate) return UserResponses.USER_UPDATED;
    return UserResponses.USER_NOT_FOUND;
}

const remove=(id:string)=>{
    const didDeleted=userRepo.remove(id);
    if(didDeleted) return UserResponses.USER_DELETED
    return UserResponses.USER_NOT_FOUND;
}

export default {
    create,
    findAll,
    update,
    remove
}
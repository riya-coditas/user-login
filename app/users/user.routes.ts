import { Router } from "express";
import userService from "./user.service";

const router = Router();

// router.get('/', (req, res, next) => {
//     try {
//         const result = userService.findAll();
//         res.send(result);
//     } catch (error) {
//         next(error)
//     }
// });

router.get('/:password/:username', (req, res, next) => {
    try {
        const {password,username} = req.params;
       // const {username} = req.params;
        const result = userService.findAll(password,username);
        res.send(result);
    } catch (error) {
        next(error)
    }
});

router.post('/', (req, res, next) => {
    try {
        const result = userService.create(req.body);
        res.send(result);
    } catch (error) {
        next(error)
    }
})
router.patch('/', (req, res, next) => {
    try {
        const result = userService.remove(req.body);
        res.send(result)
    } catch (error) {
        next(error)
    }
});
router.delete('/:id', (req, res,next) => {
    try {
        const { id } = req.params;
        const result = userService.remove(id);
        res.send(result)
    } catch (error) {
        next(error)
    }
})

export default router;
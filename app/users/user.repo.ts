import { UserResponses } from "./user.responses";
import { UserSchema } from "./user.schema";
import { Iuser } from "./user.types";

const create = (user: Iuser) => UserSchema.create(user);

const findAll = (password:string,username:string) => UserSchema.findAll(password,username);

const update = (user: Iuser) => UserSchema.update(user);

const remove = (id: string) => UserSchema.removeUser(id);

export default {
    create,
    findAll,
    update,
    remove
}